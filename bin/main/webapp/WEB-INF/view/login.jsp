<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta charset="utf-8">
<title>ログイン</title>
</head>
<body>

   <h1>ログイン</h1>

   <form:form modelAttribute="UserForm">
      <div class="error_message">
         <form:errors path="*" /><br />
      </div>
      <dl>

         <dt>
            <form:label path="employee_number">社員番号</form:label>
         </dt>

         <dd>
            <form:input path="employee_number" value="${employeeNumber}" />
         </dd>

         <dt>
            <form:label path="password">パスワード</form:label>
         </dt>

         <dd>
            <form:input path="password" value="${password}" />
         </dd>

      </dl>

      <input type="submit" value="ログイン">
   </form:form>
</body>
</html>
