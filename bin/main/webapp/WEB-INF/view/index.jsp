<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
   <meta charset="utf-8">
   <title>ホーム</title>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
</head>
<body>
	<h1>ホーム</h1>
	<a href="${pageContext.request.contextPath}/mySetting">ユーザー情報編集</a>
	<a href="${pageContext.request.contextPath}/statistics">結果分析</a>
	<a href="${pageContext.request.contextPath}/result">過去の結果を見る</a>
	<c:if test="${loginUser.departmentId == 1}">
		<a href="${pageContext.request.contextPath}/management">ユーザー管理</a>
	</c:if>
	<a href="${pageContext.request.contextPath}/logout">ログアウト</a>

	<a href="${pageContext.request.contextPath}/exam">診断する！</a>


	<!-- Drive Analysis Volunteer Creative の順で渡す -->
	<input type="hidden" value="${result.drivePoint},${result.analysisPoint},${result.volunteerPoint},${result.creativePoint}" class="examScore" />
	<canvas id="myChart" width=“300px” height=“300px”></canvas>


	<script>

		var ctx = document.getElementById('myChart').getContext('2d');
		var inputArr = $('.examScore').val();
		console.log(inputArr);
		var inputList = inputArr.split(',');
		console.log(inputList);

		var drive = parseInt(inputList[0], 10);
		var analysis = parseInt(inputList[1], 10);
		var volunteer = parseInt(inputList[2], 10);
		var creative = parseInt(inputList[3], 10);

		var myChart = new Chart(ctx, {
			type : 'radar',
			data : {
		      labels : [ 'Drive', 'Analysis', 'Volunteer', 'Creative' ],
		      datasets : [ {

			  label : 'label',
			  data : [ drive, analysis, volunteer, creative ],
			  backgroundColor : "rgba(255,0,0,0.4)"

			  } ]
		    }
		});

	</script>



</body>
</html>