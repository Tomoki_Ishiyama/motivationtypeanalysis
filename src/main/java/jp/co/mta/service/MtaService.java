package jp.co.mta.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import jp.co.mta.dto.UserDto;
import jp.co.mta.entity.User;
import jp.co.mta.form.UserForm;
import jp.co.mta.mapper.MtaMapper;

public class MtaService {

	@Autowired
	private MtaMapper mtaMapper;

	public void signUpMta(UserForm userForm) {
		User user = new User();
		BeanUtils.copyProperties(user, userForm);
		mtaMapper.signUpMta(user);
		return;

	}

	public User loginMta(UserForm userForm) {
		User user = new User();
		BeanUtils.copyProperties(user, userForm);
		mtaMapper.loginMta(user);
		return user;

	}

	private UserDto convertToDto(User user) {
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(user, userDto);

		return userDto;
	}

}
