package jp.co.mta.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import jp.co.mta.entity.User;
import jp.co.mta.form.UserForm;
import jp.co.mta.service.MtaService;

@Controller
@SessionAttributes(value="loginUser")
public class MtaController {


	@Autowired
	private MtaService mtaService;

	@RequestMapping(value = "/sigunup", method = RequestMethod.GET)
	public String InsertUser(Model model) {
	    UserForm userForm = new UserForm();
	    model.addAttribute("userForm", userForm);
	    return "sigunup";
	}

	@RequestMapping(value = "/sigunup", method = RequestMethod.POST)
	public String InsertUser(@ModelAttribute UserForm userForm, Model model) {
	    mtaService.signUpMta(userForm);
	    return "redirect:/management";
	}


	@RequestMapping(value ="/login",method=RequestMethod.GET)
	public String login(HttpServletRequest request, Model model) {
		UserForm form = new UserForm();
		form.setEmployeeNumber(0);
		form.setPassword(null);
		model.addAttribute("userForm", form);
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String getFormInfo(@Valid @ModelAttribute UserForm form, BindingResult result, Model model) {

		if (result.hasErrors()) {
			model.addAttribute("message", "エラー");
			return "./login";
		}

		User user = mtaService.loginMta(form);

		//We have to check how to use @
		List<String> errorMessages = new ArrayList<String>();
		if(user == null) {
			errorMessages.add("nullエラー！");
			return "./login";
		}

		//save data to session
		setRequestForm(form);
		return "redirect:./";
	}
//
//	@RequestMapping(value = "/login", method = RequestMethod.POST)
//	public String request2(@ModelAttribute("loginUser")UserForm form, BindingResult result, Model model){
//
//		//      ここに　ここにログインの処理
//
//
//		//   	List<String> errorMessages = new ArrayList<String>();
//
//		return "redirect:./";
//	}

	@ModelAttribute("loginUser")
	public UserForm setRequestForm(UserForm requestForm){
		return requestForm;
	}
}
