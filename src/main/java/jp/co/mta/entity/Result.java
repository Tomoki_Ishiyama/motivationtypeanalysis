package jp.co.mta.entity;

import java.util.Date;

public class Result {

	private int id;
	private String name;
	private String department;
	private int drivePoint;
	private int analysisPoint;
	private int createPoint;
	private int volunteerPoint;
	private Date createdDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public int getDrivePoint() {
		return drivePoint;
	}
	public void setDrivePoint(int drivePoint) {
		this.drivePoint = drivePoint;
	}
	public int getAnalysisPoint() {
		return analysisPoint;
	}
	public void setAnalysisPoint(int analysisPoint) {
		this.analysisPoint = analysisPoint;
	}
	public int getCreatePoint() {
		return createPoint;
	}
	public void setCreatePoint(int createPoint) {
		this.createPoint = createPoint;
	}
	public int getVolunteerPoint() {
		return volunteerPoint;
	}
	public void setVolunteerPoint(int volunteerPoint) {
		this.volunteerPoint = volunteerPoint;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
