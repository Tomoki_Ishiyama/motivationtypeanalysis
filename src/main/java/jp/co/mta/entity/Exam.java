package jp.co.mta.entity;

public class Exam {

	private int test01_1;
	private int test01_2;
	private int test02_1;
	private int test02_2;
	private int test03_1;
	private int test03_2;
	private int test04_1;
	private int test04_2;
	private int test05_1;
	private int test05_2;
	private int test06_1;
	private int test06_2;
	private int test07_1;
	private int test07_2;
	private int test08_1;
	private int test08_2;
	private int test09_1;
	private int test09_2;
	private int test10_1;
	private int test10_2;

	public int getTest01_1() {
		return test01_1;
	}
	public void setTest01_1(int test01_1) {
		this.test01_1 = test01_1;
	}
	public int getTest01_2() {
		return test01_2;
	}
	public void setTest01_2(int test01_2) {
		this.test01_2 = test01_2;
	}
	public int getTest02_1() {
		return test02_1;
	}
	public void setTest02_1(int test02_1) {
		this.test02_1 = test02_1;
	}
	public int getTest02_2() {
		return test02_2;
	}
	public void setTest02_2(int test02_2) {
		this.test02_2 = test02_2;
	}
	public int getTest03_1() {
		return test03_1;
	}
	public void setTest03_1(int test03_1) {
		this.test03_1 = test03_1;
	}
	public int getTest03_2() {
		return test03_2;
	}
	public void setTest03_2(int test03_2) {
		this.test03_2 = test03_2;
	}
	public int getTest04_1() {
		return test04_1;
	}
	public void setTest04_1(int test04_1) {
		this.test04_1 = test04_1;
	}
	public int getTest04_2() {
		return test04_2;
	}
	public void setTest04_2(int test04_2) {
		this.test04_2 = test04_2;
	}
	public int getTest05_1() {
		return test05_1;
	}
	public void setTest05_1(int test05_1) {
		this.test05_1 = test05_1;
	}
	public int getTest05_2() {
		return test05_2;
	}
	public void setTest05_2(int test05_2) {
		this.test05_2 = test05_2;
	}
	public int getTest06_1() {
		return test06_1;
	}
	public void setTest06_1(int test06_1) {
		this.test06_1 = test06_1;
	}
	public int getTest06_2() {
		return test06_2;
	}
	public void setTest06_2(int test06_2) {
		this.test06_2 = test06_2;
	}
	public int getTest07_1() {
		return test07_1;
	}
	public void setTest07_1(int test07_1) {
		this.test07_1 = test07_1;
	}
	public int getTest07_2() {
		return test07_2;
	}
	public void setTest07_2(int test07_2) {
		this.test07_2 = test07_2;
	}
	public int getTest08_1() {
		return test08_1;
	}
	public void setTest08_1(int test08_1) {
		this.test08_1 = test08_1;
	}
	public int getTest08_2() {
		return test08_2;
	}
	public void setTest08_2(int test08_2) {
		this.test08_2 = test08_2;
	}
	public int getTest09_1() {
		return test09_1;
	}
	public void setTest09_1(int test09_1) {
		this.test09_1 = test09_1;
	}
	public int getTest09_2() {
		return test09_2;
	}
	public void setTest09_2(int test09_2) {
		this.test09_2 = test09_2;
	}
	public int getTest10_1() {
		return test10_1;
	}
	public void setTest10_1(int test10_1) {
		this.test10_1 = test10_1;
	}
	public int getTest10_2() {
		return test10_2;
	}
	public void setTest10_2(int test10_2) {
		this.test10_2 = test10_2;
	}

}
