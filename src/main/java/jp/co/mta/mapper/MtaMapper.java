<<<<<<< HEAD
package jp.co.mta.mapper;

import java.util.List;

import jp.co.mta.dto.DepartmentDto;
import jp.co.mta.entity.Result;
import jp.co.mta.entity.User;

public interface MtaMapper {

	DepartmentDto selectDepartment();

	User loginMta(User user);

	void signUpMta(User user);

	List<Result> selectResult();

	List<Result> selectResultByDepartment(int departmentId);

	List<Result> selectResultByName(String name);

	List<User> selectAllUser();

	List<User> selectUserById(int id);

	void updateUser(User user);

	void deleteUser(int id);

	void stopUser(User user);

	void finishUser(User user);

}
=======
>>>>>>> 37f1fb2150e2f806d5401a7f3a95fb78d5c4815d
