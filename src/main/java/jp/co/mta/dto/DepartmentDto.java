package jp.co.mta.dto;

public class DepartmentDto {

	private int id;
	private String name;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDepartment() {
		return name;
	}
	public void setDepartment(String department) {
		this.name = department;
	}

}
